module.exports = function(grunt) {
    grunt.initConfig({
        karma: {
            unit: {
                options: {
                    configFile: 'test/jasmine/conf/karma.conf.js',
                    singleRun: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-karma');
    grunt.registerTask('default', ['karma']);

};
