# 1. INTRODUCTION

This is the code for the Educational First Company Interview, it is based on the specifications and assets
received from the company.

The project weight is bigger than necessary because I have used several technologies for a small project like this.

I have used this technologies because I think that was important to:

1. Show the creation of a "real world" big project structure: Mainteinable and scalable, using the latest technologies.
2. Show my technical skills on different roles (backend, frontend, ux, accessibility...) as part of a professional web development.
3. Show the powerful of mixing a MVC framework in the BE combined with a powerful MVC in the FE. Good skeleton
	 architecture for easy extension of the app. 
4. Create a more readable, elegant and easy understandable code.

All the view has been painted with html + css except the switch buttons.

## ** IMPORTANT NOTES: **

### About the send Messages:
I have not made the send messages part of the view because I think it was not very logical because:

1. Is not usable change the position of switches that are in movement.
2. If the messages are ignored and doesn't change the values of the data received, the switches are going to it's natural position (received in the JSON) instantly.
3. I can't wait to a period of time in which the app doesn't sends anything because the conection is closed by the server and if I make a reconnection, the websocket sends again the JSONS responses.

I have added a send example to the main controller of the code to show how to manage them.

### Folk Raphael.js library
I have folked and modified a third party library named raphael.js to customize the gauges.

### About the comments
You can find all the documentation of the different functions and modules of the application in the code.
The documentation is compatible with docular format (http://grunt-docular.com/)

##Main technologies used

### - Backend:

** Server:** node.js   
** Framework: ** ExpressJS   
** Task Runner: ** grunt  

### - Frontend:

** Templating: ** Jade and html5   
** Framework: ** AngularJS, jQuery   
** Stylesheet: ** CSS3  
** Testing: ** Jasmine and Karma 
I wanted to use less for create more maintainable stylesheets, grid960 or bootstrap griding for floating elements
but it requires more time.

### - Comunication:

** Remote API: ** WebSockets.   
Socket.io has been installed and prepared for use but in that test waa unnecessary communicate the BE with the FE.

# 2. INSTALLATION

For install the project, first clone this repository:

> git clone https://javierprietodiaz@bitbucket.org/javierprietodiaz/ef-test.git

go to root directory of the project.

install dependencies:
> npm install package.json

Execute the server:

> node app.js

With the web browser, access to:
> http://localhost:<port showed in the console>

# 3. EXECUTING TESTS
To execute the FE test you have to use grunt, go to the root directory of the project and execute:

> grunt



