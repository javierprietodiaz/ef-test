/**
 * @description
 *
 * ## App Server
 *
 * This is the server app module.
 * This function is responsible of:
 *   - Require dependencies.
 *   - Setup environment.
 *   - Routing.
 */

/**
 * Module dependencies.
 */

var express = require('express')
  , routes  = require('./controllers')
  , server  = require('http')
  , path 	= require('path')
  , io		= require('socket.io');	
  
var app = express();

// all environments
app.set('port', process.env.PORT || 8080);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//ROUTES
app.get('/', routes.index);

// Server INIT listening
var httpServer = server.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

