/**
 * @doc function
 * @name exports:index
 * @description
 *
 * ## App Main controller
 * Routing handlers for the main page
 */



// GET home page.
exports.index = function (req, res, next) {
  return res.render('index', {title: 'EF TEST APPLICATION'})
}
