/**
 * @doc controller
 * @name AppCtrl
 * @description
 *
 * ## App Main controller
 *
 * This is the main controller of the app.
 * This function is responsible of:
 *   - Conection with the websockets
 *   - Initialize the gauges.
 *   - Get and refresh the telemetry data of the view.
 *   - Movement of the gauges and switches.
 */

feModule.controller('AppCtrl', ['$scope', '$window',  function ($scope, $window) {
    var     speedAr      = []
        ,   altitudeAr   = []
        ,   speedSum     = 0
        ,   altitudeSum  = 0
        ,   $flapsSwitch = $(".flaps-switch")
        ,   socketAddr
        ,   ws;

    //Dynamic height for centered-container (adapts it to the window height at loading the page)
    $scope.contHeight = $window.innerHeight - 250 + 'px';

    // Websocket Connection
    socketAddr = "ws://ec2-79-125-71-146.eu-west-1.compute.amazonaws.com:8888/telemetry";
    ws = new utils.WebSocketHndl(socketAddr, $scope, "wsTelemetry");
    ws.send("example message sended to websocket"); //example of message sending to ws

    //--------------- GAUGES INIT ---------------//
    /* Velocity */
    var vel_gauge = Raphael.fn.speedometer("velocity-gauge",240,240,0); 
    vel_gauge.position(0);
    vel_gauge.setOptions({
        plateColor: "#212121",
        color1: "#FFFFFF",
        color2: "#FFFFFF",  
        needleColor:"#A7A7A7",
        needleHubColor: "#A7A7A7",
        startAngle: 90,
        endAngle: 540,
        startNumber: 0,
        endNumber: 500,
        minorTickWidth: 0,
        majorTickWidth: 0,
        bezelThickness: 0,
        textPadding: 13,
        odometerText: ""
    });
    
    /* Altitude */
    var alt_gauge = Raphael.fn.speedometer("altitude-gauge",240,240,0); 
    alt_gauge.position(0);
    alt_gauge.setOptions({
        plateColor: "#212121",
        color1: "#FFFFFF",
        color2: "#FFFFFF",  
        needleColor:"#A7A7A7",
        needleHubColor: "#A7A7A7",
        startAngle: 90,
        endAngle: 540,
        startNumber: 0,
        endNumber: 10,
        minorTickWidth: 0,
        majorTickWidth: 0,
        bezelThickness: 0,
        textPadding: 10,
        odometerText: ""
    });
    
    // Watcher for changes in telemetry data
    $scope.$watch('wsTelemetry.rcv_data', function() {
        if($scope.wsTelemetry.rcv_data.telemetry) {
            /* Speed */
            if ($scope.wsTelemetry.rcv_data.telemetry.airspeed < 500 && 
                $scope.wsTelemetry.rcv_data.telemetry.airspeed > 0) {   //Remove illogical data
                
                speedAr.push($scope.wsTelemetry.rcv_data.telemetry.airspeed);

                vel_gauge.accelerate($scope.wsTelemetry.rcv_data.telemetry.airspeed); //Gauge mov
                $scope.max_velocity = Math.max.apply(Math, speedAr); //Max
                $scope.min_velocity = Math.min.apply(Math, speedAr); //Min

                speedSum += $scope.wsTelemetry.rcv_data.telemetry.airspeed; //Avg
                $scope.avg_velocity = (speedSum / speedAr.length).toFixed(2);
            }

            /* Altitude */

            if ($scope.wsTelemetry.rcv_data.telemetry.altitude < 99999 && 
                $scope.wsTelemetry.rcv_data.telemetry.altitude > 0) {   //Remove illogical data

                altitudeAr.push($scope.wsTelemetry.rcv_data.telemetry.altitude);

                alt_gauge.accelerate($scope.wsTelemetry.rcv_data.telemetry.altitude / 10000); //Gauge mov

                $scope.max_altitude = Math.max.apply(Math, altitudeAr);
                $scope.min_altitude = Math.min.apply(Math, altitudeAr);

                altitudeSum += $scope.wsTelemetry.rcv_data.telemetry.altitude; //Avg
                $scope.avg_altitude = (altitudeSum / altitudeAr.length).toFixed(2);
            }
        }
        if ($scope.wsTelemetry.rcv_data.control) {
            /* Landing Gear */
            $scope.landing_gear = $scope.wsTelemetry.rcv_data.control.landing_gear;

            /* Flaps */
            moveFlapsSwitch($scope.wsTelemetry.rcv_data.control.flaps);
        }
    });

    //---------- PRIVATE FUNCTS --------//

    /**
     * @doc function
     * @name moveFlapsSwitch
     * @param  {number} value Position of the flaps
     *
     * @description
     * This function is responsible of actualize the flaps switch
     * to the position obtained from the base station.
     */
    function moveFlapsSwitch(value) {
        $flapsSwitch 
            .css("transition-duration", "1s")
            .css("transition-timing-function", "linear");

        switch ($scope.wsTelemetry.rcv_data.control.flaps) {
            case 1:
                $flapsSwitch.css("transform", "translateX(22px)");
                break;
            case 2:
                $flapsSwitch.css("transform", "translateX(46px)");
                break;
            case 3:
                $flapsSwitch.css("transform", "translateX(72px)");
                break;
            case 4:
                $flapsSwitch.css("transform", "translateX(97px)");
                break;
            case 5:
                $flapsSwitch.css("transform", "translateX(120px)");
                break;
            default:
                $flapsSwitch.css("transform", "translateX(0px)");
                break; 
        }
    }
}]);