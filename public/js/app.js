/**
 * @doc module
 * @name feModule
 * @description
 *
 * ## App Main controller
 *
 * This is the main module of the app.
 */
var feModule = angular.module('feApp', []);
