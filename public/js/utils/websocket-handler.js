//utils namespace
var utils = utils || {};

/**
 * @doc class
 * @name utils.global:WebsocketHndl
 *
 * @param  {string} addrs Address to connect the websocket
 * @param  {object} scope The scope context where you want to use the
 * websocket.
 * @param  {string} name Name of the scope variable of the scope context
 * to bind the interesting data related with the websocket.
 *
 * @return {object} self(this) returns an interface with the class to send messages
 * to the websocket.
 * EXAMPLE: 
 *   var ws = utils.WebSocketHndl
 *   ws.send("Message to send"); 
 *
 * @description
 * This class is used to connect to a websocket and get data information
 * related with it.
 * 
 * This information is attached to a scope variable passed as argument
 * scope[name]
 *
 * This lets you to use in your scope context and watch the variable to
 * actualize the data in your view.
 *
 */
utils.WebSocketHndl = function (addrs, scope, name){
    self = this;

    //Default vals
    scope[name] = {
        'rcv_data': {},
        'status': 0
    };

    //Websocket not supported by browser
    if(!"WebSocket" in window) {
        alert("WebSocket NOT supported by your Browser!");
    }
    else {
        // Opens a web socket
        var ws = new WebSocket(addrs);

        //Connection Stablished
        ws.onopen = function (msg) {
            scope[name].status = ws.readyState;
            scope.$apply();
        };
        //Receiving message
        ws.onmessage = function (event) {
            var json;
            if (/^[\],:{}\s]*$/.test((event.data).replace(/\\["\\\/bfnrtu]/g, '@').
                replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
                replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                //the json is ok
                scope[name].rcv_data = JSON.parse(event.data);
            }
            scope[name].status =  ws.readyState;
            scope.$apply();
        };

        //Connection Closed
        ws.onclose = function() { 
            // websocket is closed.
            scope[name].status =  ws.readyState;
            console.log("Connection is closed...");
            scope.$apply();
        };

        /* PUBLIC METHODS */

        //Send message to websocket
        self.send = function (msg) {
            ws.onopen = function () {
                ws.send(msg);
            };  
        }
    }
    return self;
};
