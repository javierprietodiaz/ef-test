
basePath = '../';

frameworks = ['jasmine'];

files = [
    // Jasmine libs
    JASMINE,
    JASMINE_ADAPTER,
    // External libs
    // -----------------------------------------------------------------------
    '../../public/js/lib/jquery/jquery-1.8.3.min.js',
    '../../public/js/lib/angular/angular.js',
    '../../public/js/lib/angular/angular-mocks-1.0.7.js',
    '../../public/js/lib/raphael/raphael.min.js',
    '../../public/js/lib/raphael/raphael.speedometer.js',
    // Source files
    // -----------------------------------------------------------------------
    
    //Module app
    '../../public/js/app.js',

    // main-controller
    '../../public/js/controllers.js',
    // Test files
    // -----------------------------------------------------------------------
    //'spec/**/*.js',
    'spec/main-controller-spec.js'
];

// if true, it capture browsers, run tests and exit (jenkins integration)
singleRun = true;

autoWatch = false;

//browsers = ['phantomjs'];
browsers = [];

reporters = ['dots', 'junit', 'coverage'];

preprocessors = {
    '**/!(*Spec).js': 'coverage'
};

coverageReporter = {
  type: 'cobertura',
  dir: 'coverage/',
  file: 'karma-coverage.xml'
};

plugins = ['karma-jasmine', 'karma-phantomjs-launcher'];
